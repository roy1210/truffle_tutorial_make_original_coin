pragma solidity ^0.4.17;

import "zeppelin-solidity/contracts/token/ERC20/StandardToken.sol";

contract NoteToken is StandardToken {
    string public name = 'NoteToken' ;
    string public symbol ='NT' ;
    uint8 public decimals = 2 ;
    uint public INITIAL_SUPPLY = 12000 ;

    function NoteToken() public{
        totalSupply_ = INITIAL_SUPPLY;
        balances[msg.sender] = INITIAL_SUPPLY ;
    }
}

//Contract と同名の関数がイニシャライザーとして認識される。この関数はイニシャライザーなので、これを実行しているのはデプロイしている人です。なので、この一行はデプロイした人にトークンを振り込んでいるのです。 1. トークンの供給量を設定。2. デプロイした人の口座に供給量のすべてのトークンを振り込む
