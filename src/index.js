import Web3 from 'web3'

const provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545')
const web3 = new Web3(provider)

web3.eth.getAccounts().then((accounts) => {
  console.log(accounts[0])
})

//web3 には web3.eth.getAccounts() というメソッド が提供されている。
//非同期処理: 非同期処理とは、時間がかかる処理に使われる。つまり、時間がかかる処理結果を持ってくる前に、前もって別の動作を先に実行しておく。肝なのは、then のあとのコードは web3.eth.getAccounts() の処理が終わった段階で実行される。
